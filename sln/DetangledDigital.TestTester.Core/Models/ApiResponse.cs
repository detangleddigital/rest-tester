﻿using System.Net;

namespace DetangledDigital.TestTester.Core.Models
{
    public class ApiResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Headers { get; set; }
        public string Content { get; set; }
        public string ContentType { get; set; }
    }
}