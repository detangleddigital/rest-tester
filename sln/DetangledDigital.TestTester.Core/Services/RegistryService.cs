﻿using System;
using Microsoft.Win32;

namespace DetangledDigital.TestTester.Core.Services
{
    public class RegistryService
    {
        private readonly string _registryPath;

        public RegistryService(string keyPath)
        {
            _registryPath = EnsureRegistryPath(keyPath);
        }

        private string EnsureRegistryPath(string keyPath)
        {
            var keyPathParts = keyPath.Split(new[] {'\\'}, StringSplitOptions.RemoveEmptyEntries);

            var rk = Registry.LocalMachine.OpenSubKey("SOFTWARE", true);

            foreach (var keyPathPart in keyPathParts)
            {
                if (rk.OpenSubKey(keyPathPart, true) == null)
                    rk.CreateSubKey(keyPathPart, RegistryKeyPermissionCheck.ReadWriteSubTree);

                rk = rk.OpenSubKey(keyPathPart, true);
            }

            return rk.Name;
        }

        public string GetValue(string key)
        {
            return Registry.GetValue(_registryPath, key, string.Empty).ToString();
        }

        public void SetValue(string key, object value)
        {
            Registry.SetValue(_registryPath, key, value);
        }
    }
}