﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using DetangledDigital.TestTester.Core.Models;
using Newtonsoft.Json;

namespace DetangledDigital.TestTester.Core.Services
{
    public class AuthenticationService
    {
        public async Task<TokenResponse> Login(string baseAddress, string endpointAddress, string username,
            string password)
        {
            var address = $"{baseAddress}/{endpointAddress}";
            HttpResponseMessage response;

            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password)
            };
            var content = new FormUrlEncodedContent(pairs);

            using (var client = new HttpClient())
            {
                var tokenEndpoint = new Uri(address);
                response = await client.PostAsync(tokenEndpoint, content);
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Error: {responseContent}");
            }

            var tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(responseContent);
            return tokenResponse;
        }
    }
}