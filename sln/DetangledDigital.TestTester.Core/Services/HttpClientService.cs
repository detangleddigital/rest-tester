﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DetangledDigital.TestTester.Core.Models;
using Newtonsoft.Json;

namespace DetangledDigital.TestTester.Core.Services
{
    public class HttpClientService
    {
        public async Task<ApiResponse> MakeRequest(string baseAddress,
            string endpointAddress,
            HttpMethod method,
            string accessToken,
            string payload
            )
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);

                // Accept Header
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Add Auth Token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

                // Build request
                var request = new HttpRequestMessage(method, endpointAddress);

                // Set content if there's a payload
                if (!string.IsNullOrWhiteSpace(payload))
                    request.Content = new StringContent(payload, Encoding.UTF8, "application/json");

                // Make request and gather response
                var apiResponse = new ApiResponse();
                await client.SendAsync(request)
                    .ContinueWith(responseTask =>
                    {
                        var response = responseTask.Result;

                        apiResponse.StatusCode = response.StatusCode;
                        apiResponse.Headers = SerializeHeaders(response.Headers);
                        apiResponse.Content = response.Content.ReadAsStringAsync().Result;
                        apiResponse.ContentType = response.Content.Headers.ContentType?.MediaType;
                    });

                return apiResponse;
            }
        }

        public string SerializeHeaders(HttpHeaders headers)
        {
            var dict = new Dictionary<string, string>();

            foreach (var item in headers.ToList())
            {
                if (item.Value == null) continue;

                var header = string.Empty;
                foreach (var value in item.Value)
                    header += value + " ";

                // Trim the trailing space and add item to the dictionary
                header = header.TrimEnd(" ".ToCharArray());
                dict.Add(item.Key, header);
            }

            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }
    }
}