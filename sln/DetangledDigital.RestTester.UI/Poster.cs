﻿using System;
using System.Drawing;
using System.Net.Http;
using System.Windows.Forms;
using DetangledDigital.RestTester.UI.App;
using DetangledDigital.TestTester.Core.Services;
using JsonPrettyPrinterPlus;

namespace DetangledDigital.RestTester.UI
{
    public partial class Poster : Form
    {
        private const string RegistryKeyPath = @"DetangledDigital\RestTester";
        private readonly RegistryService _registryService;

        public Poster()
        {
            InitializeComponent();

            _registryService = new RegistryService(RegistryKeyPath);

            PopulateFormControls();
            LoadPreviousValues();
        }

        private void PopulateFormControls()
        {
            cmbRequestMethod.Items.Add(new ComboboxItem {Text = "GET", Value = HttpMethod.Get});
            cmbRequestMethod.Items.Add(new ComboboxItem {Text = "POST", Value = HttpMethod.Post});
            cmbRequestMethod.Items.Add(new ComboboxItem {Text = "PUT", Value = HttpMethod.Put});
            cmbRequestMethod.Items.Add(new ComboboxItem {Text = "DELETE", Value = HttpMethod.Delete});

            cmbRequestMethod.SelectedIndex = 0;
        }

        private void LoadPreviousValues()
        {
            txtApiBaseAddress.Text = _registryService.GetValue("ApiBaseAddress");
            txtAuthenticationUri.Text = _registryService.GetValue("AuthenticationUri");
            txtUsername.Text = _registryService.GetValue("Username");
            txtPassword.Text = _registryService.GetValue("Password");
            txtRequestEndpoint.Text = _registryService.GetValue("RequestEndpoint");

            txtRequestEndpoint.Text = _registryService.GetValue("RequestEndpoint");
            var method = _registryService.GetValue("RequestMethod");
            if (!string.IsNullOrWhiteSpace(method))
                cmbRequestMethod.SelectedIndex = cmbRequestMethod.FindString(method);
            txtRequestContent.Text = _registryService.GetValue("RequestContent");
        }

        private async void btnGetToken_Click(object sender, EventArgs e)
        {
            try
            {
                PleaseWaitFormManager.ShowPleaseWait();

                SaveAuthenticationFormValues();

                var authenticationService = new AuthenticationService();

                var result = await authenticationService.Login(txtApiBaseAddress.Text,
                    txtAuthenticationUri.Text,
                    txtUsername.Text,
                    txtPassword.Text);

                txtToken.Text = result.AccessToken;
                grpRequest.Enabled = true;
            }
            catch (Exception ex)
            {
                grpRequest.Enabled = false;
                txtToken.Text = string.Empty;
                PleaseWaitFormManager.CloseForm();

                MessageBox.Show($"Authentication Error{Environment.NewLine}{ex.Message}",
                    "Authentication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                PleaseWaitFormManager.CloseForm();
            }
        }

        private void SaveAuthenticationFormValues()
        {
            _registryService.SetValue("ApiBaseAddress", txtApiBaseAddress.Text);
            _registryService.SetValue("AuthenticationUri", txtAuthenticationUri.Text);
            _registryService.SetValue("Username", txtUsername.Text);
            _registryService.SetValue("Password", txtPassword.Text);
            _registryService.SetValue("RequestEndpoint", txtRequestEndpoint.Text);
        }

        private async void btnMakeRequest_Click(object sender, EventArgs e)
        {
            try
            {
                PleaseWaitFormManager.ShowPleaseWait();

                SaveRequestFormValues();

                var clientService = new HttpClientService();

                var result = await clientService.MakeRequest(txtApiBaseAddress.Text,
                    txtRequestEndpoint.Text,
                    (cmbRequestMethod.SelectedItem as ComboboxItem).Value as HttpMethod,
                    txtToken.Text,
                    txtRequestContent.Text);

                grpResponse.Enabled = true;
                txtResponseStatusCode.Text = $"({(int) result.StatusCode}) {result.StatusCode}";
                
                if ((int)result.StatusCode < 300)
                    txtResponseStatusCode.ForeColor = Color.Green;
                else if ((int) result.StatusCode < 400)
                    txtResponseStatusCode.ForeColor = Color.OrangeRed;
                else
                    txtResponseStatusCode.ForeColor = Color.Red;

                txtContentType.Text = result.ContentType;
                txtResponseHeaders.Text = result.Headers;
                txtResponseContent.Text = result.Content.PrettyPrintJson();
            }
            catch (Exception ex)
            {
                txtResponseStatusCode.ForeColor = Color.Black;
                grpResponse.Enabled = false;
                txtResponseStatusCode.Text = string.Empty;
                txtContentType.Text = string.Empty;
                txtResponseHeaders.Text = string.Empty;
                txtResponseContent.Text = string.Empty;

                PleaseWaitFormManager.CloseForm();

                MessageBox.Show($"Request Error:{Environment.NewLine}{ex.Message}",
                    "Request Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                PleaseWaitFormManager.CloseForm();
            }
        }

        private void SaveRequestFormValues()
        {
            _registryService.SetValue("RequestEndpoint", txtRequestEndpoint.Text);
            _registryService.SetValue("RequestMethod", (cmbRequestMethod.SelectedItem as ComboboxItem).Text);
            _registryService.SetValue("RequestContent", txtRequestContent.Text);
        }

        private void txtToken_TextChanged(object sender, EventArgs e)
        {
            grpRequest.Enabled = !string.IsNullOrWhiteSpace(txtToken.Text);
        }
    }
}