﻿using System.Threading;
using System.Windows.Forms;

namespace DetangledDigital.RestTester.UI.App
{
    internal class PleaseWaitFormManager
    {
        //The type of form to be displayed as the splash screen.
        private static PleaseWait _pleaseWaitForm;

        public static void ShowPleaseWait()
        {
            if (_pleaseWaitForm != null && !_pleaseWaitForm.IsDisposed) return;
            var thread = new Thread(ShowForm);

            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();

            Thread.Sleep(700);
        }

        private static void ShowForm()
        {
            _pleaseWaitForm = new PleaseWait();
            _pleaseWaitForm.TopMost = true;
            _pleaseWaitForm.StartPosition = FormStartPosition.CenterScreen;
            _pleaseWaitForm.Show();
            _pleaseWaitForm.Refresh();

            Application.Run(_pleaseWaitForm);
        }

        public static void CloseForm()
        {
            if (_pleaseWaitForm == null) return;
            _pleaseWaitForm.Invoke(new CloseDelegate(CloseFormInternal));
        }

        private static void CloseFormInternal()
        {
            if(_pleaseWaitForm == null) return;
            _pleaseWaitForm.Close();
            _pleaseWaitForm = null;
        }

        //Delegate for cross thread call to close
        private delegate void CloseDelegate();
    }
}