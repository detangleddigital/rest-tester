﻿namespace DetangledDigital.RestTester.UI
{
    partial class Poster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label8;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Poster));
            this.grpAuthentication = new System.Windows.Forms.GroupBox();
            this.btnGetToken = new System.Windows.Forms.Button();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtAuthenticationUri = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtApiBaseAddress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.grpRequest = new System.Windows.Forms.GroupBox();
            this.btnMakeRequest = new System.Windows.Forms.Button();
            this.txtRequestContent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbRequestMethod = new System.Windows.Forms.ComboBox();
            this.txtRequestEndpoint = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grpResponse = new System.Windows.Forms.GroupBox();
            this.txtContentType = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtResponseStatusCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtResponseContent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtResponseHeaders = new System.Windows.Forms.TextBox();
            label8 = new System.Windows.Forms.Label();
            this.grpAuthentication.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpRequest.SuspendLayout();
            this.grpResponse.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(6, 136);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(44, 13);
            label8.TabIndex = 21;
            label8.Text = "Content";
            // 
            // grpAuthentication
            // 
            this.grpAuthentication.Controls.Add(this.btnGetToken);
            this.grpAuthentication.Controls.Add(this.txtToken);
            this.grpAuthentication.Controls.Add(this.txtPassword);
            this.grpAuthentication.Controls.Add(this.label2);
            this.grpAuthentication.Controls.Add(this.txtUsername);
            this.grpAuthentication.Controls.Add(this.lblUsername);
            this.grpAuthentication.Controls.Add(this.txtAuthenticationUri);
            this.grpAuthentication.Controls.Add(this.label1);
            this.grpAuthentication.Location = new System.Drawing.Point(12, 81);
            this.grpAuthentication.Name = "grpAuthentication";
            this.grpAuthentication.Size = new System.Drawing.Size(1000, 142);
            this.grpAuthentication.TabIndex = 0;
            this.grpAuthentication.TabStop = false;
            this.grpAuthentication.Text = "API Authentication";
            // 
            // btnGetToken
            // 
            this.btnGetToken.Location = new System.Drawing.Point(907, 23);
            this.btnGetToken.Name = "btnGetToken";
            this.btnGetToken.Size = new System.Drawing.Size(75, 23);
            this.btnGetToken.TabIndex = 10;
            this.btnGetToken.Text = "Get Token";
            this.btnGetToken.UseVisualStyleBackColor = true;
            this.btnGetToken.Click += new System.EventHandler(this.btnGetToken_Click);
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(9, 73);
            this.txtToken.Multiline = true;
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(973, 52);
            this.txtToken.TabIndex = 5;
            this.txtToken.TextChanged += new System.EventHandler(this.txtToken_TextChanged);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(696, 26);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(182, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(642, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Password";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(437, 26);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(182, 20);
            this.txtUsername.TabIndex = 3;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(383, 29);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(55, 13);
            this.lblUsername.TabIndex = 5;
            this.lblUsername.Text = "Username";
            // 
            // txtAuthenticationUri
            // 
            this.txtAuthenticationUri.Location = new System.Drawing.Point(103, 26);
            this.txtAuthenticationUri.Name = "txtAuthenticationUri";
            this.txtAuthenticationUri.Size = new System.Drawing.Size(242, 20);
            this.txtAuthenticationUri.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Authentication Uri";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtApiBaseAddress);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1000, 63);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "API Info";
            // 
            // txtApiBaseAddress
            // 
            this.txtApiBaseAddress.Location = new System.Drawing.Point(103, 26);
            this.txtApiBaseAddress.Name = "txtApiBaseAddress";
            this.txtApiBaseAddress.Size = new System.Drawing.Size(242, 20);
            this.txtApiBaseAddress.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "API Base Uri";
            // 
            // grpRequest
            // 
            this.grpRequest.Controls.Add(this.btnMakeRequest);
            this.grpRequest.Controls.Add(this.txtRequestContent);
            this.grpRequest.Controls.Add(this.label6);
            this.grpRequest.Controls.Add(this.cmbRequestMethod);
            this.grpRequest.Controls.Add(this.txtRequestEndpoint);
            this.grpRequest.Controls.Add(this.label3);
            this.grpRequest.Enabled = false;
            this.grpRequest.Location = new System.Drawing.Point(12, 230);
            this.grpRequest.Name = "grpRequest";
            this.grpRequest.Size = new System.Drawing.Size(1000, 185);
            this.grpRequest.TabIndex = 12;
            this.grpRequest.TabStop = false;
            this.grpRequest.Text = "API Request";
            // 
            // btnMakeRequest
            // 
            this.btnMakeRequest.Location = new System.Drawing.Point(857, 31);
            this.btnMakeRequest.Name = "btnMakeRequest";
            this.btnMakeRequest.Size = new System.Drawing.Size(125, 23);
            this.btnMakeRequest.TabIndex = 18;
            this.btnMakeRequest.Text = "Make Request";
            this.btnMakeRequest.UseVisualStyleBackColor = true;
            this.btnMakeRequest.Click += new System.EventHandler(this.btnMakeRequest_Click);
            // 
            // txtRequestContent
            // 
            this.txtRequestContent.Location = new System.Drawing.Point(104, 60);
            this.txtRequestContent.Multiline = true;
            this.txtRequestContent.Name = "txtRequestContent";
            this.txtRequestContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRequestContent.Size = new System.Drawing.Size(878, 113);
            this.txtRequestContent.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Content";
            // 
            // cmbRequestMethod
            // 
            this.cmbRequestMethod.FormattingEnabled = true;
            this.cmbRequestMethod.Location = new System.Drawing.Point(730, 33);
            this.cmbRequestMethod.Name = "cmbRequestMethod";
            this.cmbRequestMethod.Size = new System.Drawing.Size(121, 21);
            this.cmbRequestMethod.TabIndex = 13;
            // 
            // txtRequestEndpoint
            // 
            this.txtRequestEndpoint.Location = new System.Drawing.Point(103, 33);
            this.txtRequestEndpoint.Name = "txtRequestEndpoint";
            this.txtRequestEndpoint.Size = new System.Drawing.Size(621, 20);
            this.txtRequestEndpoint.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "API Uri";
            // 
            // grpResponse
            // 
            this.grpResponse.Controls.Add(this.txtContentType);
            this.grpResponse.Controls.Add(this.label4);
            this.grpResponse.Controls.Add(this.txtResponseStatusCode);
            this.grpResponse.Controls.Add(this.label9);
            this.grpResponse.Controls.Add(label8);
            this.grpResponse.Controls.Add(this.txtResponseContent);
            this.grpResponse.Controls.Add(this.label7);
            this.grpResponse.Controls.Add(this.txtResponseHeaders);
            this.grpResponse.Enabled = false;
            this.grpResponse.Location = new System.Drawing.Point(12, 432);
            this.grpResponse.Name = "grpResponse";
            this.grpResponse.Size = new System.Drawing.Size(1000, 299);
            this.grpResponse.TabIndex = 13;
            this.grpResponse.TabStop = false;
            this.grpResponse.Text = "API Response";
            // 
            // txtContentType
            // 
            this.txtContentType.Location = new System.Drawing.Point(460, 35);
            this.txtContentType.Name = "txtContentType";
            this.txtContentType.Size = new System.Drawing.Size(242, 20);
            this.txtContentType.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(383, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Content Type";
            // 
            // txtResponseStatusCode
            // 
            this.txtResponseStatusCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResponseStatusCode.Location = new System.Drawing.Point(104, 34);
            this.txtResponseStatusCode.Name = "txtResponseStatusCode";
            this.txtResponseStatusCode.Size = new System.Drawing.Size(242, 20);
            this.txtResponseStatusCode.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Status Code";
            // 
            // txtResponseContent
            // 
            this.txtResponseContent.Location = new System.Drawing.Point(103, 133);
            this.txtResponseContent.Multiline = true;
            this.txtResponseContent.Name = "txtResponseContent";
            this.txtResponseContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResponseContent.Size = new System.Drawing.Size(879, 145);
            this.txtResponseContent.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Headers";
            // 
            // txtResponseHeaders
            // 
            this.txtResponseHeaders.Location = new System.Drawing.Point(103, 60);
            this.txtResponseHeaders.Multiline = true;
            this.txtResponseHeaders.Name = "txtResponseHeaders";
            this.txtResponseHeaders.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResponseHeaders.Size = new System.Drawing.Size(879, 67);
            this.txtResponseHeaders.TabIndex = 11;
            // 
            // Poster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.grpResponse);
            this.Controls.Add(this.grpRequest);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpAuthentication);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Poster";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Web API Tester";
            this.grpAuthentication.ResumeLayout(false);
            this.grpAuthentication.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpRequest.ResumeLayout(false);
            this.grpRequest.PerformLayout();
            this.grpResponse.ResumeLayout(false);
            this.grpResponse.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAuthentication;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtAuthenticationUri;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetToken;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtApiBaseAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox grpRequest;
        private System.Windows.Forms.TextBox txtRequestEndpoint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbRequestMethod;
        private System.Windows.Forms.TextBox txtRequestContent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnMakeRequest;
        private System.Windows.Forms.GroupBox grpResponse;
        private System.Windows.Forms.TextBox txtResponseContent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtResponseHeaders;
        private System.Windows.Forms.TextBox txtResponseStatusCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtContentType;
        private System.Windows.Forms.Label label4;
    }
}

